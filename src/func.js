const getSum = (str1, str2) => {


  if (typeof str1 != 'string' || typeof str2 != 'string' || str1.length == 0 || str2.length == 0){
    return false
  }
  if (str1.length == '') {
    return str2;
  }
  if (str2 == '') {
    return str1;
  }
  for (const el of str1) {
    if (isNaN(el)){
      return false
    }
  }
  for (const el of str1) {
    if (isNaN(el)){
      return false
    }
  }
  let res = ''
  var toadd = [str1, str2].sort((b, a) => a.length - b.length)
  var c = 0
  for (let index = 0; index < str2.length; index++) {
    var sum = parseInt(toadd[0][toadd[0].length-1-index])  + parseInt(toadd[1][toadd[1].length-1-index]);
    if ( sum > 9){
      res += ((sum+c) % 10) + '';
      c = 1;
    }else{
      res += (sum+c) + '';
      c = 0;
    }
  }
  res = res.split('').reverse().join('');
  var fin = ''
  for (let i = 0; i < toadd[0].length - toadd[1].length; i++){
    fin += toadd[0][i]
  }
  return fin + res;


};


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var comments = []
  var ps = 0;
  var cm = 0;

  for (const post of listOfPosts) {
    if(post.author == authorName){
      ps++;
    }
    if (post.comments){
      for (const el of post.comments) {
        comments.push(el);
      }
      
    }
    
  }
  for (const com of comments) {
    if (com.author == authorName){
      cm++;
    }
  }
  return 'Post:' + ps + ',comments:' + cm;
};

const tickets=(people)=> {
  var pocket = 0;
  for (const client  of people) {
    if(pocket >= client-25){
      pocket += 25
    } else{
      return 'NO'
    }
  }
  return 'YES'
    
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
